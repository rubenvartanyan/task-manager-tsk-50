package ru.vartanyan.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.service.IReceiverService;
import ru.vartanyan.tm.listener.LogMessageListener;
import ru.vartanyan.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final IReceiverService receiverService = new ActiveMQConnectionService();
        receiverService.receive(new LogMessageListener());
    }

}
