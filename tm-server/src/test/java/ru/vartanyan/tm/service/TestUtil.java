package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.service.dto.UserService;

import java.util.List;

public class TestUtil {

    @NotNull
    static final IPropertyService propertyService = new PropertyService();

    @NotNull
    static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    static final IUserService userService = new UserService(propertyService, connectionService);

    public static void initUser() {
        userService.create("test", "test", "test@test.ru");
        userService.create("test2", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

}
