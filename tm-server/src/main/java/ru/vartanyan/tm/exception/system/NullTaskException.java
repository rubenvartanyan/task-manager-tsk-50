package ru.vartanyan.tm.exception.system;

public class NullTaskException extends Exception {

    public NullTaskException() {
        super("Error! TaskGraph is not found...");
    }

}