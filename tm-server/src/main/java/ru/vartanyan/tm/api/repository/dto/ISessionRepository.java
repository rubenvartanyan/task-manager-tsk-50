package ru.vartanyan.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.dto.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void clear();

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}
