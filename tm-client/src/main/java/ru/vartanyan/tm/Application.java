package ru.vartanyan.tm;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.util.SystemUtil;

public class Application {

    public static void main(String[] args) throws Throwable {
        System.out.println(SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
